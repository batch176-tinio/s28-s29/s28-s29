// [OBJECTIVE] Create a server-side app using Express Web Framework.



// Relate this task to something that you do on a daily basis.

// [SECTION] Append the entire app to our node package manager.

	// package.json -> the heart of every node project. This also contains different metadata that describes the structure of the project.

	// scripts -> used to declare and describe custom commands and keyword that can be used to execute this project with the correct runtime environment.

	// NOTE: "start" is globally recognized amongst node projects and frameworks as the 'default' command script to execute a task/project. However, for the *unconventional* keywords or command you have to append the command 'run'.

	// Syntax: npm run <custom command> 

// 1. Identify and prepare ingredients.
const express = require("express");




// express -> will be used as the main component to create the server.
// We need to be able to gather/acquire the utilities and components needed that the express library will provide us.
	// => require() -> directive used to get the library/component needed inside the module.

	// prepare the environment in which the project will be served.

// [SECTION] Preparing a remote repository for our Node project.

	// NOTE: Always DISABLE the node_modules folder. This is because:
		// 1. It will take up to much space in our repository and making it a lot more difficult to stage upon committing the changes in our remote repo.
		// 2. If ever that you will deploy your node project on deployment platforms like Heroku, Netlify, Vercel, the project will automatically be rejected because node modules is not recognized on various deployment platforms

	// How? using a .gitignore module


// [SECTION] Create a runtime environment that automatically autofix all the change in our app.

	// We're going to use a utility called nodemon
	// upon starting the entry point module with nodemon, you will be able to 'append' the application with proper runtime environment. Allowing you to save time and effor upon committing changes to your app.

// you can even insert items like text art into your runtime environent.
console.log(`
	Welcome to our Express API Server
		─────────▀▀▀▀▀▀──────────▀▀▀▀▀▀▀
	──────▀▀▀▀▀▀▀▀▀▀▀▀▀───▀▀▀▀▀▀▀▀▀▀▀▀▀
	────▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀──────────▀▀▀
	───▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀──────────────▀▀
	──▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀──────────────▀▀
	─▀▀▀▀▀▀▀▀▀▀▀▀───▀▀▀▀▀▀▀───────────────▀▀
	─▀▀▀▀▀▀▀▀▀▀▀─────▀▀▀▀▀▀▀──────────────▀▀
	─▀▀▀▀▀▀▀▀▀▀▀▀───▀▀▀▀▀▀▀▀──────────────▀▀
	─▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀
	─▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀
	─▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀
	──▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀
	───▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀▀
	─────▀▀▀▀▀▀▀▀▀▀▀▀▀───────────────▀▀▀
	──────▀▀▀▀▀▀▀▀▀▀▀───▀▀▀────────▀▀▀
	────────▀▀▀▀▀▀▀▀▀──▀▀▀▀▀────▀▀▀▀
	───────────▀▀▀▀▀▀───▀▀▀───▀▀▀▀
	─────────────▀▀▀▀▀─────▀▀▀▀
	────────────────▀▀▀──▀▀▀▀
	──────────────────▀▀▀▀
	───────────────────▀▀
`);







